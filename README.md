# ClamSentinel-2.0
Clam Sentinel 2.0 is a antivirus program for basic Windows 7 and newer protection. 
The program was written in python 3 based on the original Clam Sentinel project made available on Sourceforge by Andrea Russo under GPLv2 license. 
It is not a continuation or a copy of it, but a new project drawing ideas from an older project.
It is currently in beta phase and until the release of the official version, it is not recommended to use it as antivirus for everyday use.

*Since I recently moved to LInux Mint from Windows 7, I decided to stop working on Clam Sentinel 2.0. Probably this project will no longer be touched by me so it gives it to people who want to continue or develop it. Currently I will be dealing with a similar project on Linux, specifically ClamTk, invite you to follow this project and also encourage you to switch to FOSS. Greetings

------------------------------------------------------
Current functions:

 -Real-time protection with ClamAV engine

 -Simple interface in console

 -Own and effective heuristic protection

 -Possibility to remove or quarantine infected files

 -Detailed event logs

 -Manual disk scanning with ClamAV


Future plans:

 -Ransormware mitigation module

 -Ability to preload ClamAV signatures into memory permanently

 -Windows file integrity checker module 
 
 -Tray icon version of the program

Please don't be angry if this program disappoints you or alienates you with its level of difficulty. I am just learning advanced programming. If you have comments or feedback, please post a ticket in the repository or email me: simplelogin.fyppx@aleeas.com

# Information
All information on installation, use, configuration can be found in the file "Documentation.txt"

All information about the rights, authors, code used can be found in the file "About.txt"

All information on changes in the following version can be found in the "Changelogs.txt" file

# License
GNU GENERAL PUBLIC LICENSE

Copyright (c) 2023 Lukas

Version 3, 29 June 2007

Copyright © 2007 Free Software Foundation, Inc. <https://fsf.org/>